<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Browse Courses</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
		body {
			background-image: url("grey1.jpg");
		} 
		#cont2{
			font-weight: bold;
			align: center;
			background-color: #b0c4de;
			width: 100%;
			height: 100%;
			background-image: url("blue4.jpg");
			background-repeat: no-repeat;
			background-size: cover;
		}
	</style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="advisorindex.htm">Home</a>
            </div>
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Browse Courses Page
                </h1>
                <ol class="breadcrumb">
                    <li><a href="advisorindex.htm">Home</a>
                    </li>
                    <li class="active">Browse Courses</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

	<!-- Content Row -->
	<div id="cont2">
        <div class="row">
			<center>
				<h2><b>Master's Programs</b></h2>
				<br />
				<h4><b>Majors (No Concentrations)<b></h4>
			</center>
        </div>
		<br />
		<!-- Majors (No Concentration) Dropdown -->
		<center><form name="Majors">
			<select name="ddmenu_name" id="ddmenu_name" style="width: 30% !important;">
				<option value="404.html">Select Major</option>
				<optgroup label="Majors">
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-Applied-Technology.aspx">Applied Technology</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-In-Business-Information-Technology.aspx">Business Information Technology</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-In-Computational-Finance.aspx">Computational Finance</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-In-Game-Development.aspx">Computer Game Development</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-in-Computer-Science.aspx">Computer Science</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-E-Commerce-Technology.aspx">E-Commerce Technology</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-In-Health-Informatics.aspx">Health Informatics</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-in-Human-Computer-Interaction.aspx">Human Computer Interaction</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-in-IT-Project-Management.aspx">IT Project Management</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-MS-in-Network-Engineering-and-Security.aspx">Network Engineering and Security</option>
				<option value="http://www.cdm.depaul.edu/academics/Pages/Current/Requirements-jdms-joint-degree.aspx">Computer Science Technology</option>
				</optgroup>
			</select>
			<input type="button" name="Submit" value="Go!" onClick="window.open(ddmenu_name.value,'newtab'+ddmenu_name.value)">
		</form></center>
		<!-- /.row -->
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<center>
			<h4><b>Majors (With Concentrations)</b></h4>
		</center>
		<br />
		<!-- Majors (With Concentrations) Dropdown -->
		<center><form name="Majors">
			<select name="ddmenu_name" id="ddmenu_name" style="width: 30% !important;">
				<option value="404.html">Select Major</option>
				<optgroup label="(Computer, Information, Network Security)">
				<option value="">Computer Security</option>
				<option value="">Governance, Risk Management, and Compliance</option>
				<option value="">Network Security</option>
				</optgroup>

				<optgroup label="(Information Systems)">
				<option value="">Business Analysis/Systems Analysis</option>
				<option value="">Business Intelligence</option>
				<option value="">Database Administration</option>
				<option value="">IT Enterprise Management</option>
				<option value="">Standard</option>
				</optgroup>
				
				<optgroup label="(Predictive Analytics)">
				<option value="">Computational Methods</option>
				<option value="">Health Care</option>
				<option value="">Hospitality</option>
				<option value="">Marketing</option>
				</optgroup>
				
				<optgroup label="(Software Engineering)">
				<option value="">Entrepreneurship & Technology Leadership</option>
				<option value="">Real-Time Game Systems</option>
				<option value="">Project Management</option>
				<option value="">Software Architecture</option>
				<option value="">Software Development</option>
				</optgroup>
			</select>
			<input type="button" name="Submit" value="Go!" onClick="window.open(ddmenu_name.value,'newtab'+ddmenu_name.value)">
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
			<br />
		</form>
		</center>
	</div>
		<!-- /.row -->
	<!-- /.row -->

        <hr style=margin-top:5%>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p><strong>Copyright &copy; When-If Masters Degree Report 2015<strong></p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>