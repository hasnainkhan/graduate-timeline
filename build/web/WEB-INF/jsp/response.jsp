 

 
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html xmlns:c="http://java.sun.com/jstl/core">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
 

    <body>
        <jsp:useBean id="mybean" scope="session" class="handler.Handler" />
        <jsp:setProperty name="mybean" property="type" />
        <jsp:setProperty name="mybean" property="firstName" />
        <jsp:setProperty name="mybean" property="lastName" />
        <jsp:setProperty name="mybean" property="userName" />
        <jsp:setProperty name="mybean" property="password" />
        <jsp:setProperty name="mybean" property="birthDay" />
        <jsp:setProperty name="mybean" property="birthMonth" />
        <jsp:setProperty name="mybean" property="birthYear" />
         <jsp:setProperty name="mybean" property="iAm" />
         <jsp:setProperty name="mybean" property="login" />
         <jsp:setProperty name="mybean" property="registration" />
         <jsp:setProperty name="mybean" property="page" />
         
       
        <h1>First Name, <jsp:getProperty name="mybean" property="firstName" />!</h1>
        <h1>Last Name, <jsp:getProperty name="mybean" property="lastName" />!</h1>
        <h1>Username, <jsp:getProperty name="mybean" property="userName" />!</h1>
        <h1>Password <jsp:getProperty name="mybean" property="password" />!</h1>
        <h1>BirthDay <jsp:getProperty name="mybean" property="birthDay" />!</h1>
        <h1>BirthMonth <jsp:getProperty name="mybean" property="birthMonth" />!</h1>
        <h1>BirthYear <jsp:getProperty name="mybean" property="birthYear" />!</h1>
        <h1>I am:  <jsp:getProperty name="mybean" property="iAm" />!</h1>
         <h1>Page type:  <jsp:getProperty name="mybean" property="type" />!</h1>
           <h1>is it login:  <jsp:getProperty name="mybean" property="login" />!</h1>
              <h1>is it registration ::  <jsp:getProperty name="mybean" property="registration" />!</h1>
   

        <% mybean.context();   %>
     
      
            
        <c:choose>
 
                 <c:when test="${param.page eq 'whatif' }">
       
        
        <c:redirect url="whatIfResult.htm" >
            <c:param name="firstName" value="${mybean.firstName}"/>
            <c:param name="lastName" value="${mybean.lastName}"/>            
        </c:redirect>        
        
    </c:when>
            
            
            <c:when test="${mybean.login && mybean.valid && mybean.getiAm() eq 'student'}">
        You are a true user
       
        
        <c:redirect url="studentindex.jsp" >
            <c:param name="firstName" value="${mybean.firstName}"/>
            <c:param name="lastName" value="${mybean.lastName}"/>
            <c:param name="major" value="${mybean.major}"/>
            
        </c:redirect>
        
        
    </c:when>
        
        <c:when test="${mybean.login && !mybean.valid}">
            
           
 
        <c:redirect url="login.htm" >
            <c:param name="inout" value="false"/>
             
             </c:redirect>
    </c:when>
        
        <c:when test="${mybean.login && mybean.valid && mybean.getiAm() eq 'advisor'}">
        You are a true user and a advisor
       
        
        <c:redirect url="advisorindex.htm" >
            <c:param name="firstName" value="${mybean.firstName}"/>
            <c:param name="lastName" value="${mybean.lastName}"/>            
        </c:redirect>        
        
    </c:when>
        
        
     
        
       
    <c:otherwise>
      <c:redirect url="login.htm" >
          
            <c:param name="inout" value="none"/>
             <c:param name="msg" value="Account Created! Go Ahead and Login"/>
             </c:redirect>
        
    </c:otherwise>
</c:choose>
   
        <jsp:setProperty name="mybean" property="valid" value="false"/>



    </body>
 
</html>
 