<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Lookup Student</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
		body {
			background-image: url("grey1.jpg");
		} 
		#cont2{
			align: center;
			font-weight: bold;
			background-color: #b0c4de;
			width: 100%;
			height: 100%;
			background-image: url("blue4.jpg");
			background-repeat: no-repeat;
			background-size: cover;
		}
	</style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="advisorindex.htm">Home</a>
            </div>
            
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Student Look-Up
                </h1>
                <ol class="breadcrumb">
                    <li><a href="advisorindex.htm">Home</a></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

	<!-- Content Row -->
	<div id="cont2">
        <div class="row">
			<center>
				<h2><b>Student Look-Up</b></h2>
			</center>
        </div>
		<br />
		<br />
		
		<form>
		 <center>
		  First Name:<br />
		  <input type="text" name="firstname">
		  <br />
		  Last Name:<br />
		  <input type="text" name="lastname">
		  <br />
		  Student ID:<br />
		  <input type="text" name="studentid">
		  <br />
		  <br />
		  <br />
			<input type="submit" value="Submit">
		  <br />
		  <br />
	</div>
		  
	<!-- /.row -->

        <hr style=margin-top:5%>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p><strong>Copyright &copy; When-If Masters Degree Report 2015</strong></p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>