/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;

/**
 *
 * @author hasnain
 */
public class Handler {
    

    
    private  Majors[] listOfMajors;
    
    public Handler() {
      
       this.listOfMajors = Majors.values();
        
        
    }
 

    /**
     * @return the listOfMajors
     */
    public Majors[] getListOfMajors() {
        
      
        return listOfMajors;
    }

    /**
     * @param listOfMajors the listOfMajors to set
     */
    public void setListOfMajors(Majors[] listOfMajors) {
        this.listOfMajors = listOfMajors;
    }
    
    
    
}
