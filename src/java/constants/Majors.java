package constants;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

public enum Majors {

    BIT {
                @Override
                public String toString() {
                    return "Business Information Technology";
                }

                @Override
                public String value() {
                    return "BIT";

                }

                @Override
                public boolean hasConcentrations() {
                    return false;
                }

                @Override
                public List<String> concentrations() {
                    return null;
                }

                @Override
                public String getConcentration(String conIn) {
                    return null;
                }
            }, Animation {
                @Override
                public String toString() {
                    return "Animation";
                }

                @Override
                public String value() {
                    return "Animation";

                }

                @Override
                public boolean hasConcentrations() {
                    return true;
                }

                @Override
                public List<String> concentrations() {
                    List<String> listOfC = Arrays.asList("Animator", "Technical Artist");
                    return listOfC;
                }

                @Override
                public String getConcentration(String conIn) {
                    if (concentrations().contains(conIn)) {
                        return conIn;
                    } else {
                        return "";
                    }

                }

            }, Cinema {
                @Override
                public String toString() {
                    return "Cinema Production";
                }

                @Override
                public String value() {
                    return "Cinema";

                }

                @Override
                public boolean hasConcentrations() {
                    return true;
                }

                @Override
                public List<String> concentrations() {
                    List<String> listOfC = Arrays.asList("Production", "Post-Production", "Sound");
                    return listOfC;
                }

                @Override
                public String getConcentration(String conIn) {
                    if (concentrations().contains(conIn)) {
                        return conIn;
                    } else {
                        return "";
                    }

                }

            };

    public abstract String value();

    public abstract boolean hasConcentrations();

    public abstract List<String> concentrations();

    public abstract String getConcentration(String concentration);

    public static final EnumSet<Majors> ALL_MAJORS = EnumSet.allOf(Majors.class);

}
