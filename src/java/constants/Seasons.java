package constants;

import java.util.EnumSet;

 

public class Seasons {
	
	public enum Quarter{
	   Fall {
        @Override
        public String toString() {
            return "0. Fall";
        }

		@Override
		public String value() {
			return "Fall";
			
		}
		
		@Override
		public int intValue() {
			return 0;
			
		}
     }, Winter {
         @Override
         public String toString() {
             return "1. Winter";
         }
         @Override
 		public String value() {
 			return "Winter";
 			
 		}
         @Override
 		public int intValue() {
 			return 1;
 			
 		}
         
     }, Spring {
         @Override
         public String toString() {
             return "2. Spring";
         }

         @Override
  		public String value() {
  			return "Spring";
  			
  		}
         @Override
 		public int intValue() {
 			return 2;
 			
 		}
      
     
     };
     
	public abstract String value(); 
	public abstract int intValue();
 }
	
	 public static final EnumSet<Quarter> ALL_SEASONS = EnumSet.allOf(Quarter.class);


}
