package graduatetimeline.constants;

import java.util.EnumSet;

 

public class Majors {
	
	public enum CDM {
	   BIT {
        @Override
        public String toString() {
            return "0. Business Information Technology";
        }

		@Override
		public String value() {
			return "BIT";
			
		}
     }, Animation {
         @Override
         public String toString() {
             return "1. Animation";
         }
         @Override
 		public String value() {
 			return "Animation";
 			
 		}
         
     }, Cinema {
         @Override
         public String toString() {
             return "2. Cinema Production";
         }

         @Override
  		public String value() {
  			return "Cinema";
  			
  		}
      
     
     };
     
	public abstract String value(); 
 }
	
	 public static final EnumSet<CDM> ALL_MAJORS = EnumSet.allOf(CDM.class);


}
