package graduatetimeline.constants;

import java.util.EnumSet;

 

public class Seasons {
	
	public enum Quarter{
	   Fall {
        @Override
        public String toString() {
            return "0. Fall";
        }

		@Override
		public String value() {
			return "Fall";
			
		}
     }, Winter {
         @Override
         public String toString() {
             return "1. Winter";
         }
         @Override
 		public String value() {
 			return "Winter";
 			
 		}
         
     }, Spring {
         @Override
         public String toString() {
             return "2. Spring";
         }

         @Override
  		public String value() {
  			return "Spring";
  			
  		}
      
     
     };
     
	public abstract String value(); 
 }
	
	 public static final EnumSet<Quarter> ALL_SEASONS = EnumSet.allOf(Quarter.class);


}
