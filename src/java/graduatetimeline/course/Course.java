package graduatetimeline.course;

import graduatetimeline.constants.Majors.CDM;

public class Course {
	
	private String subject;
	private int catalogNbr;
	private String majorName;
	private String concentration;
	private boolean offeredFall;
	private boolean offeredWinter; 
	private boolean offeredSpring;
	private boolean safe_fall_starter;
	private boolean safe_winter_starter;
	private boolean safe_spring_starter;
	private String preq;
	private boolean elective;
	private boolean capstone;
	private String comments;
	private String url;
	
	public Course(String subject, int catalogNbr,String majorName,String concentration,
	  boolean offeredFall, boolean offeredWinter, boolean offeredSpring, 
	  boolean safe_fall_starter,  
	  boolean safe_winter_starter, boolean safe_spring_starter,  
	  String preq,  boolean elective,  boolean capstone, String comments, String url){
		
		setSubject(subject);
		setCatalogNbr(catalogNbr);
		setMajorName(majorName);
		setConcentration(concentration);
		setOfferedFall(offeredFall);
		setOfferedWinter(offeredWinter);
		setOfferedSpring(offeredSpring);
		setSafeFallStarter(safe_fall_starter);
		setSafeWinterStarter(safe_winter_starter);
		setSafeSpringStarter(safe_spring_starter);
		setPreq(preq);
		setElective(elective);
		setCapstone(capstone);
		setComments(comments);
		setUrl(url);
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
		
	}
	
	public void setCatalogNbr(int catalogNbr) {
		this.catalogNbr = catalogNbr;
		
	}
	
   
   public void setMajorName(String majorName){
	   this.majorName=majorName;
   }
   
   public void setConcentration(String conIn) {
		this.concentration = conIn;
		
	}
   
   public void setOfferedFall(boolean offeredFall){
	   this.offeredFall = offeredFall;
	   
   }
   public void setOfferedWinter(boolean offeredWinter){
	   this.offeredWinter = offeredWinter;
   }
   public void setOfferedSpring(boolean offeredSpring){
	   this.offeredSpring = offeredSpring;
	   
   }
   public void setSafeFallStarter(boolean safe_fall_starter){
	   this.safe_fall_starter = safe_fall_starter;
	   
   }
   public void setSafeWinterStarter(boolean safe_winter_starter){
	   this.safe_winter_starter = safe_winter_starter;
	   
   }
   public void setSafeSpringStarter(boolean safe_spring_starter){
	   this.safe_spring_starter = safe_spring_starter;
	   
   }
   public void setPreq(String preq) {
	   this.preq = preq;
	   
   }
   public void setElective(boolean elective) {
	   this.elective = elective;
	   
   }
   public void setCapstone(boolean capstone) {
	   this.capstone = capstone;
	   
   }
   public void setComments(String comments) {
	   this.comments = comments;
	   
   }
   public void setUrl(String url){
	   this.url = url;
	   
   }
   
   public String getSubject() {
		return this.subject ;
		
	}
	
	public int getCatalogNbr( ) {
		 return  this.catalogNbr  ;
		
	}
	
  
  public String getMajorName( ){
	   return this.majorName;
  }
  
  public String getConcentration(){
		return this.getConcentration();
	}
  
  public boolean getOfferedFall( ){
	  return  this.offeredFall  ;
	   
  }
  public boolean  getOfferedWinter(){
	  return  this.offeredWinter;
  }
  public boolean getOfferedSpring(){
	  return  this.offeredSpring  ;
	   
  }
  public boolean getSafeFallStarter( ){
	  return  this.safe_fall_starter ;
	   
  }
  public boolean  getSafeWinterStarter( ){
	   return this.safe_winter_starter ;
	   
  }
  public boolean getSafeSpringStarter( ){
	   return this.safe_spring_starter  ;
	   
  }
  public String getPreq( ) {
	  return  this.preq  ;
	   
  }
  public boolean getElective( ) {
	   return this.elective;
	   
  }
  public boolean getCapstone( ) {
	   return this.capstone ;
	   
  }
  public String getComments( ) {
	   return this.comments ;
	   
  }
  public String getUrl(   ){
	   return this.url ;
	   
  }
	 
}
