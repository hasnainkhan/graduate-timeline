package graduatetimeline.course;

 
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import graduatetimeline.constants.Majors.CDM;
 

public class CourseFactory {
	
	public static LinkedHashMap<String, Course> allCourse = new LinkedHashMap<String, Course>();

	public static LinkedHashMap<String,  LinkedList<Course>> majorChain = new LinkedHashMap< >();
	 
	public static final CourseFactory instance = new CourseFactory();
	 
	protected CourseFactory() { //singleton 
	}
 
 
	public static CourseFactory getInstance() {
		return instance;
	}
	
	public void showCourses(){
		
		for (String key : this.allCourse.keySet()) {
	        System.out.println(key + " " + this.allCourse.get(key));
	        System.out.println("value is " +this.allCourse.get(key).getSubject() +""+this.allCourse.get(key).getCatalogNbr());
	    }
		 

	     
		
		 
	}
	
	public String toString() {
		System.out.println("Course Chain");
		Set<Map.Entry<String,Course>> pairs = getCourseChain().entrySet();
		for (Map.Entry<String,Course> e :pairs){
			System.out.println(e);
			
		}
		
		System.out.println("Major Chain");
		
		Set<Map.Entry<String,LinkedList<Course>>> pairs2 = this.majorChain.entrySet();
		for (Map.Entry<String,LinkedList<Course>> e :pairs2){
			System.out.println("Key is: "+e);
			Iterator<Course> it = e.getValue().iterator();
			while(it.hasNext()){
				Course now = it.next();
				System.out.println(now.getSubject()+String.valueOf(now.getCatalogNbr()));
			}
			
		}
		return "";
		
	}
	
	public LinkedHashMap<String, Course>  getCourseChain(){
		return this.allCourse;
	}
	
	public LinkedList<Course> getMajorChain(String keyIn){
		return this.majorChain.get(keyIn);
	}
	
	
	public Course makeCourse(String subject, int catalogNbr, String majorName, String concentration,
			  boolean offeredFall, boolean offeredWinter, boolean offeredSpring, 
			  boolean safe_fall_starter,  
			  boolean safe_winter_starter, boolean safe_spring_starter,  
			  String preq,  boolean elective,  boolean capstone, String comments, String url){
		
		
		String id = subject+String.valueOf(catalogNbr);
		String majorId = majorName+ concentration;
                  //  majorId = majorId.replace(" ","");
		if (allCourse.get(id) != null) {//course exists
			return allCourse.get(id); // return course in linkedhashmap
		}
		
		
		
		Course newCourse = new Course( subject,   catalogNbr,   majorName, concentration,
				    offeredFall,   offeredWinter,  offeredSpring, 
				   safe_fall_starter,  
				    safe_winter_starter,  safe_spring_starter,  
				    preq,   elective,   capstone,  comments,  url);
		allCourse.put(id, newCourse);
		
		 if (!this.majorChain.containsKey(majorId)){ //does not contain the major name
			 LinkedList<Course> newList = new LinkedList<>();// create new linked list
			 this.majorChain.put(majorId, newList);
		 }
		
		 this.majorChain.get(majorId).add(newCourse);
		
		return newCourse;
		
	}
	
	
}
