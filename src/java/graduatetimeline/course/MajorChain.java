package graduatetimeline.course;

import java.util.ArrayList;

import graduatetimeline.constants.Majors.CDM;

public class MajorChain {
	public String concentration;
	public CDM majorName;
	
	public ArrayList<Course> coursesByMajor = new ArrayList<>();
	
	public MajorChain(String conIn, CDM majorNameIn) {
		setConcentration(conIn);
		setMajorName(majorNameIn);
	}
		 
		
		
	public void setConcentration(String conIn) {
		this.concentration = conIn;
		
	}
	
	public void setMajorName(CDM majorNameIn){
		this.majorName = majorNameIn;

}
	public String getConcentration(){
		return this.getConcentration();
	}
	
	public CDM getMajorName() {
		
		return this.majorName;
	}
	
	public void addCourseToMajor(Course courseIn) {
		this.getCoursesByMajor().add(courseIn);
		
	}
	
	public ArrayList<Course> getCoursesByMajor(){
		return this.getCoursesByMajor();
	}
	
}
