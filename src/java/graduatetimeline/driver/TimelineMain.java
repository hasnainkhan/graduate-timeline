package graduatetimeline.driver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;

import graduatetimeline.constants.Majors;
import graduatetimeline.constants.Majors.CDM;
import graduatetimeline.constants.Seasons;
import graduatetimeline.course.Course;
import graduatetimeline.course.CourseFactory;
import exceptions.NoCSVFoundException;
import exceptions.QuarterFullException;

import graduatetimeline.quarter.Quarter;
import graduatetimeline.quarter.QuarterFactory;
import graduatetimeline.student.Student;
import graduatetimeline.timeline.Timeline;
import java.io.File;

public class TimelineMain {

    static Student grad;

    static ArrayList<Quarter> view = new ArrayList<>();


    /*public static void main(String[] args) throws Exception {
     createStudent("Applied Technology", "Applied Information Technology Specialization", "Fall", "1");
	
	
     }
     */
    public static int printMajorOptions() throws IOException {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        for (CDM ALL_MAJORS : Majors.ALL_MAJORS) {
            System.out.println(ALL_MAJORS);
        }

        System.out.print("Choose Major by Number:");
        int major = Integer.parseInt(read.readLine());
        return major;
    }

    public static int printStartQuarter() throws IOException {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        for (Seasons.Quarter ALL_SEASONS : Seasons.ALL_SEASONS) {
            System.out.println(ALL_SEASONS);
        }

        System.out.print("Choose Start Quarter by Number:");
        int startQuarter = Integer.parseInt(read.readLine());
        return startQuarter;
    }

    public static void createStudent(String majorin, String concentration, String startQuarterin, String coursesPerQurater) throws Exception {

        Seasons.Quarter startQuarter = null;

        Seasons.Quarter[] arraySeasons = Seasons.Quarter.values();
        for (Seasons.Quarter in : arraySeasons) {
            if (in.name().equals(startQuarterin)) {
                startQuarter = in;
                break;
            }
        }

        grad = new Student(majorin, "Bob", Integer.parseInt(coursesPerQurater), concentration, startQuarter, 2015);
        try {
            grad.json();
        } catch (Exception ex) {
            throw new NoCSVFoundException();
        }
        String key = (grad.getMajor() + grad.getConcentration());
        //  key  = key.replace(" ", "");
        createQuarter(key, grad.getStartQuarter());

    }

    public static void createQuarter(String key, Seasons.Quarter quarterIn) throws QuarterFullException, FileNotFoundException, UnsupportedEncodingException {

// CourseFactory.getInstance().toString();
        Iterator<Course> majorIterator = CourseFactory.getInstance().getMajorChain(key).iterator();
        Timeline.getInstance().createTimeline(key,
                grad.getConcentration(), grad.getCoursePerQuarter());

        int startYear = 2015;
        int startQuarter = 0;
        String quarterName = quarterIn.name();
        int courseCounter = 0;
        String qPosition = grad.getStudentName() + grad.getMajorId() + grad.getConcentration() + grad.getStartQuarter();

        clearConsole();
        while (majorIterator.hasNext()) {

            String quarterId = quarterName + startYear + grad.getMajor() + grad.getCoursePerQuarter();
            Course itCourse = majorIterator.next();
            System.out.println(itCourse.getSubject() + " " + itCourse.getCatalogNbr());

            Quarter newQuarter = QuarterFactory.getInstance().makeQuarter(quarterName, grad.getConcentration(),
                    grad.getMajor(), grad.getCoursePerQuarter(), startYear);

            newQuarter.information();

            newQuarter.addCourse(itCourse);;

            newQuarter.information();

            if (newQuarter.isCourseCartFull()) {
                Timeline.getInstance().addToTimeLine(qPosition, newQuarter);
                startQuarter++;

            }

            switch (quarterName) {
                case "Fall":
                    quarterName = "Winter";
                    startYear++;
                    break;
                case "Winter":
                    quarterName = "Spring";

                    break;
                case "Spring":
                    quarterName = "Fall";
                    break;

            }

        }
 
        Timeline.getInstance().writeSchedule(qPosition);
     

        
    }

    public final static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (final Exception e) {
            //  Handle any exceptions.
        }
    }

}
