package graduatetimeline.quarter;
import graduatetimeline.course.Course;
import exceptions.QuarterFullException;

import java.util.ArrayList;

import graduatetimeline.constants.Majors.CDM;

public class Quarter {

	private String quarter;
	private String major;
	private int coursePerQuarter;
	private int year;
	private String concentration;
	//private Course[] courseCart = new Course [coursePerQuarter];
	private ArrayList<Course> courseCart = new ArrayList<>();
	
	public Quarter(String startQIn, String concentration, String majorIn, int coursePerQIn, int yearIn){
		
		 setStartQuarter(startQIn);
		 setMajor(majorIn);
		 setConcentration(concentration);
		 setCoursePerQuarter(coursePerQIn );
		 setYear(yearIn);
		 
		  
	}
	
	
	public void setYear(int year){
		this.year = year;
	}
	public void setStartQuarter(String startQIn) {
		this.quarter = startQIn;
	}
	
	public void setConcentration(String conIn) {
		this.concentration = conIn;
		
	}
	
	public void setMajor(String majorIn) {
		this.major = majorIn;
	}
	
	public void setCoursePerQuarter(int coursePerQIn) {
		this.coursePerQuarter = coursePerQIn;
	}
	
	public String getMajor( ) {
		return this.major;
	}
	
	public String getQuarter() {
		return this.quarter;
	}
	
	public int getCoursePerQuarter( ) {
		return this.coursePerQuarter;
	}
	
	public String getConcentration(){
		return this.concentration;
	}
	
	public void addCourse(Course courseIn) throws QuarterFullException {
		if (this.getCourseCart().size()>= this.getCoursePerQuarter())
			throw new QuarterFullException();
		this.getCourseCart().add(courseIn);
	}
	
	public ArrayList<Course> getCourseCart( ) {
		
		return this.courseCart;
		
	}
	
	public StringBuilder information() {
		StringBuilder builder = new StringBuilder();


		builder.append("\n------------------------------------------- <br/>" );
	//	builder.append("<h3>\nMajor: "+this.getMajor() + "<br/></h3>");
                    //                builder.append(" \n<h4>Concentration: "+this.getConcentration() + "</h5><br/>");
		builder.append("\n"+String.valueOf(this.getYear()) + " " + this.getQuarter() + "<br/>");
		builder.append("\nCourses per quarter: "+ this.getCoursePerQuarter() + "<br/>");
		builder.append("\nCourse slots left: " + (this.getCoursePerQuarter() - this.getCourseCart().size()) + "<br/>");
		
		builder.append("\nCourses in this Quarter" + "<br/>");
		
		for (Course in: this.getCourseCart()){
			builder.append("\n"+in.getSubject()+" "+ in.getCatalogNbr() + "<br/>");
		}
		
		System.out.println(builder.toString());
	return builder;
	}
	
	public String id() {
		
		return this.getMajor() + this.getQuarter() + this.getCoursePerQuarter();
	}
	
	public boolean isCourseCartFull( ) {
		
		if (this.getCourseCart().size()>=this.getCoursePerQuarter())
			return true;
		else
			return false;
		
	}
	
	public int getYear(){
		return this.year;
	}
}
