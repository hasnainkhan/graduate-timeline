/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graduatetimeline.student;

 import java.io.File;
import java.io.FileInputStream;
 

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
 

import graduatetimeline.constants.Majors.CDM;
import graduatetimeline.constants.Seasons;
import graduatetimeline.course.CourseFactory;
import exceptions.NoCSVFoundException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
 
public class Student {
	private String major;
	private String name;
	private int coursePerQuarter;
	private String concentration;
	private Seasons.Quarter startQuarter;
	private int startYear;
	
	public Student(String majorIn, String nameIn, int coursePerQIn, String concentration,
			Seasons.Quarter startQuarter, int yearIn){
		 setCourse(majorIn);
		 setStudentName(nameIn);
		 setCoursePerQuarter(coursePerQIn );
		 setConcentration(concentration);
		 setStartQuarter(startQuarter);
                                     setStartYear(yearIn);
		 
		
	}
	
	public void setStartQuarter(Seasons.Quarter startQuarter) {
		this.startQuarter = startQuarter;
	}
	
	public void setCourse(String majorIn) {
		this.major = majorIn;
	}
	
	public void setStudentName(String nameIn) {
		this.name = nameIn;
	}
	
	public void setConcentration(String conIn) {
            
              if(conIn.equals("Standard") || conIn.equals("") || conIn == null   ) {
                  this.concentration = "";
              }
              else{
		this.concentration = conIn;
              }
		
	}
	
	public void setCoursePerQuarter(int coursePerQIn) {
		this.coursePerQuarter = coursePerQIn;
	}
	
	public String getMajor(  ) {
		return this.major;
	}
	
	public String getStudentName( ) {
		return this.name;
	}
	
	public String getConcentration(){
		return this.concentration;
	}
	
	public int getCoursePerQuarter( ) {
		return this.coursePerQuarter;
	}
	
	public String getMajorId(){
		return this.getMajor()+this.getConcentration();
	}
	
	public Seasons.Quarter getStartQuarter() {
		return this.startQuarter;
	}
	
	
	public void json() throws Exception  {
		 
	//	File f = new File(this.major.name());
            try{
             //   Path csvFile = new Path("resources/" +this.major.name()); 
                
          String json = "Majors" + File.separator+ this.major + File.separator;
        if(this.getConcentration().equals("Standard") || this.getConcentration().equals("") || this.getConcentration() == null   ) {//major folder and file name 0;
        json +="0";   
        }
        
        else{
            json += this.getConcentration();
            
        }
         
        String path =this.getClass().getClassLoader().getResource("resources"+File.separator+json).getPath();
         String fixPath =URLDecoder.decode(path, "UTF-8");
         File f = new File(fixPath); 
         
        
            

          
        if (f.exists()){
            FileInputStream is = new FileInputStream(f);
            String jsonTxt = IOUtils.toString(is);
            JSONArray jsonArray = new JSONArray(jsonTxt);  
            System.out.println(jsonTxt);
             
            for (Object inArray: jsonArray ){
    
            	if (inArray instanceof JSONObject) {
                  
                  
            		 CourseFactory.getInstance().makeCourse(
                            
            	       ((JSONObject) inArray).getString("subject") ,
                               
                                
                                 
                            ((JSONObject) inArray).getInt("catalogNbr"),
                                       String.valueOf(  this.getMajor()),
                          String.valueOf(  this.getConcentration()),
                            ((JSONObject) inArray).getBoolean("fall"),
                            ((JSONObject) inArray).getBoolean("winter"),
                            ((JSONObject) inArray).getBoolean("spring"),
                            ((JSONObject) inArray).getBoolean("fall_safe"),
                            ((JSONObject) inArray).getBoolean("winter_safe"),
                            ((JSONObject) inArray).getBoolean("spring_safe"),
                            ((JSONObject) inArray).getString("preq"),
                            // ((((JSONObject) inArray).isNull("preq")) ? "null" :  ((JSONObject) inArray).getString("preq")),

                            ((JSONObject) inArray).getBoolean("elective"),
                            ((JSONObject) inArray).getBoolean("capstone"),
                         String.valueOf(((JSONObject) inArray).get("comments")),
                           
                             String.valueOf(((JSONObject) inArray).get("url"))   );
                    
            	}
                
                 
                
            	
            }
            CourseFactory.getInstance().showCourses();
             
              try {
                  this.finalize();
              } catch (Throwable ex) {
                  Logger.getLogger(Student.class.getName()).log(Level.SEVERE, null, ex);
              }
		
	}
	
        }			//this.class.getClassLoader().getResource(this.major.name()).getPath());

            catch(Exception e){
                throw new NoCSVFoundException();
            }

}

    /**
     * @return the startYear
     */
    public int getStartYear() {
        return startYear;
    }

    /**
     * @param startYear the startYear to set
     */
    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }
            
              
	
}