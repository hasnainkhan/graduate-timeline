package graduatetimeline.timeline;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import graduatetimeline.constants.Majors.CDM;
import java.io.File;
import graduatetimeline.quarter.Quarter;
import java.io.FileOutputStream;

public class Timeline {
	
	private static LinkedHashMap<String, LinkedList<Quarter>> timeLine = new LinkedHashMap< >();
	
	private String ID;
	private String major;
	private int coursePerQuarter;
	 
	private String concentration;
	
	
	public static final Timeline instance = new Timeline();
	 
	protected Timeline() { //singleton 
	}
 
 
 
	public static Timeline getInstance() {
		return instance;
	}
	
	public void createTimeline(String majorIn, String concentration, int coursePerQuarter){
		 String majorclean = majorIn.replace(concentration, "");
                                     
		 setMajor(majorclean);
		 setConcentration(concentration);
		 setCoursePerQuarter(coursePerQuarter );
		 setID();
		
	}
	
	
	public void setID(){
		this.ID = this.getMajor()+this.getConcentration()+this.getCoursePerQuarter();
	}
	
	public String getID(){
		return this.ID;
	}
	
	public void setConcentration(String conIn) {
            if (conIn.equals("") || conIn.isEmpty())
                conIn = "Standard";
		this.concentration = conIn;
		
	}
	
	public void setMajor(String majorIn) {
		this.major = majorIn;
	}
	
	public void setCoursePerQuarter(int coursePerQIn) {
		this.coursePerQuarter = coursePerQIn;
	}
	
	public String getConcentration( ) {
		return this.concentration;
		
	}
	
	public int getCoursePerQuarter() {
		return this.coursePerQuarter ;
	}
	
	public String getMajor( ) {
		return this.major;
	}
	
	public void addToTimeLine(String qPosition, Quarter quarterIN) {
		
		 if (!this.timeLine.containsKey(qPosition)){ //does not contain the quarter
			 LinkedList<Quarter> newList = new LinkedList<>();// create new linked list
			 this.timeLine.put(qPosition, newList);
		 }
		
		 this.timeLine.get(qPosition).add(quarterIN);
		 
		
		
	}
	
	public LinkedList<Quarter> getTimeLine(String qPosition) {
		
		
		return  this.timeLine.get(qPosition);
	}
	
	public void writeSchedule(String id) throws FileNotFoundException, UnsupportedEncodingException {
          
            try {
            //    /home/hasnain/Documents/DePaul/Capstone/Demo/web/WEB-INF/jsp/output/output.txt
                String test = this.getClass().getClassLoader().getResource("resources/output/").getPath();
               // test+= this.getID()+".txt";
                test += "output.txt";
                String outputWeb =  "/home/hasnain/Documents/DePaul/Capstone/Demo/web/WEB-INF/jsp/output/output.txt";
                System.out.println(test);
                File f = new File(outputWeb);
                try {
                    if (f.createNewFile())
                        System.out.println("File Created");
                    else
                        System.out.println("File Exists");
                }
                catch (Exception e){
                    System.out.println(e.getMessage());
                }
           PrintWriter writer =     new PrintWriter(new FileOutputStream(f, false));
         //       PrintWriter writer =     new PrintWriter( f);
                writer.write("<center>");
               writer.write("<h3>\nMajor: "+this.getMajor() + "<br/></h3>");
                             writer.write(" \n<h4>Concentration: "+this.getConcentration() + "</h5><br/>");
                for (Quarter in: this.timeLine.get(id)){
                  writer.write(in.information().toString());
                   //  writer.(in.information().toString());
                    //writer.print(in.information());
                    
                    
                }
                 writer.write("</center>");
                writer.close();
                
               
             
            } catch (Exception ex) {
                  System.out.println(ex.getMessage());
            }
		
	}

}
