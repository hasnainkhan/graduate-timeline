package handler;

import registration.*;
import constants.Majors;
import static constants.Majors.ALL_MAJORS;
import constants.Quarter;
import login.LoginUser;
import login.StudentLogin;
import registration.WebUser.userType;

public class Handler {

    
    
        public enum webpage{
        
        login, registration, whatif
        
    }
    private boolean login;
    private boolean registration;
    private boolean valid;
    private String type;
    private String iAm;
    private Majors major;
    private int coursePerQuarter;
    private String concentration;
    private Quarter startQuarter;
    private int startYear;
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private String birthDay;
    private String birthMonth;
    private String birthYear;
    private WebUser user1;
    private WebUser user2;
    private LoginUser userLogin;
    private String page;

    public Handler() {
           this.iAm = null;
   this.major = null;
     this.coursePerQuarter = 0;
    this.concentration= null;
    this.startQuarter= null;
   this.startYear= 0;
    this.firstName= null;
    this.lastName= null;
    this.userName= null;
    this.password= null;
    this.birthDay= null;
    this.birthMonth= null;
    this.birthYear= null;
    this.valid = false;

    }

    /**
     * @return the iAm
     */
    public String getiAm() {
        if (this.iAm == null)
            return "";
        return iAm;
    }

    /**
     * @param iAm the iAm to set
     */
    public void setiAm(String iAm) {
        iAm = iAm.toLowerCase();
        for (userType in : userType.values()) {
            if (in.name().equals(iAm))
                this.iAm = iAm;
            
        }
      
    }
    
    /**
     * @return the thisPage
     */
    public String getPage() {
        return page;
    }

    /**
     * @param thisPage the thisPage to set
     */
    public void setPage(String page) {
        
        page = page.toLowerCase();
        for (webpage p: webpage.values()){
            if(page.equals(p.name())){
                 this.page = p.name();
                 break;
            }
        
        }
       
    }

    /**
     * @return the major
     */
    public Majors getMajor() {
        if(this.major == null)
            return Majors.BIT;
        
        return major;
    }

    /**
     * @param major the major to set
     */
    public void setMajor(Majors major) {
        this.major = major;
    }

    /**
     * @return the coursePerQuarter
     */
    public int getCoursePerQuarter() {
        if (this.coursePerQuarter ==0)
            return 0;
        return coursePerQuarter;
    }

    /**
     * @param coursePerQuarter the coursePerQuarter to set
     */
    public void setCoursePerQuarter(int coursePerQuarter) {
        this.coursePerQuarter = coursePerQuarter;
    }

    /**
     * @return the concentration
     */
    public String getConcentration() {
        if (this.concentration == null)
            return "null-CON";
        return concentration;
    }

    /**
     * @param concentration the concentration to set
     */
    public void setConcentration(String concentration) {
        this.concentration = concentration;
    }

    /**
     * @return the startQuarter
     */
    public Quarter getStartQuarter() {
        if (this.startQuarter == null)
            return Quarter.Fall;
        
        return startQuarter;
    }

    /**
     * @param startQuarter the startQuarter to set
     */
    public void setStartQuarter(Quarter startQuarter) {
        this.startQuarter = startQuarter;
    }

    /**
     * @return the startYear
     */
    public int getStartYear() {
        if (this.startQuarter== null)
            return 0;
        return startYear;
    }

    /**
     * @param startYear the startYear to set
     */
    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the birthDay
     */
    public String getBirthDay() {
        return birthDay;
    }

    /**
     * @param birthDay the birthDay to set
     */
    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    /**
     * @return the birthMonth
     */
    public String getBirthMonth() {
        return birthMonth;
    }

    /**
     * @param birthMonth the birthMonth to set
     */
    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    /**
     * @return the birthYear
     */
    public String getBirthYear() {
        return birthYear;
    }

    /**
     * @param birthYear the birthYear to set
     */
    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }
    
    public void checkUser() {
        
//        if (userexists)
//           return string;
//        else
//            context();
    }
    
    
    public void context() {
        
        if (this.isLogin()){
            loginCheck();
            
        }
        
          if (this.isRegistration()){
            register();
        }
       
        
    }
    
    
    
    public void register(){
         if (this.iAm.equals(userType.student.name()))
         user1 =new Student(this.getUserName(), this.getPassword(), this.getFirstName(),
            this.getLastName(), this.getMajor(), this.getConcentration(), this.getStartYear(),
           this.getStartQuarter(), this.getCoursePerQuarter(), this.getiAm(), this.getBirthDay(), this.getBirthMonth(), this.getBirthYear());
        try {
            user1.registerUser();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        
          //register new webUser that is of type advisor
         if (this.iAm.equals(userType.advisor.name())){
         user2 =new Advisor(this.getUserName(), this.getPassword(), this.getFirstName(),
            this.getLastName(), this.getMajor(), this.getConcentration(), this.getStartYear(),
           this.getStartQuarter(), this.getCoursePerQuarter(), this.getiAm(), this.getBirthDay(), this.getBirthMonth(), this.getBirthYear());
         try {
             //try to register new advisor
            user2.registerUser();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }         
         }
        
    }
    
    
public void loginCheck(){
    try {
   // if (this.iAm.equals(userType.student.name())){
        userLogin = new StudentLogin(this.getiAm(), this.getUserName(), this.getPassword());
        
   // }
    
        this.setValid(userLogin.vaidateLogin());
        this.setFirstName(userLogin.getFirstName());
         this.setLastName(userLogin.getLastName());
         for (Majors s :  ALL_MAJORS) {
  if (s.name().equals(userLogin.getMajor()))
      this.setMajor(s);
  
  
}
          
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    
    
}
    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the valid
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * @param valid the valid to set
     */
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    /**
     * @return the login
     */
    public boolean isLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(boolean login) {
        if (login)
            this.setRegistration(false);
        this.login = login;
    }

    /**
     * @return the registration
     */
    public boolean isRegistration() {
        return registration;
    }

    /**
     * @param registration the registration to set
     */
    public void setRegistration(boolean registration) {
        if (registration)
            this.setLogin(false);
        this.registration = registration;
    }

}
