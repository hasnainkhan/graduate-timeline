/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

/**
 *
 * @author hasnain
 */
public class IncorrectLoginException extends Exception {

    private String userName;

    public IncorrectLoginException(String userName) {
        this.userName = userName;
    }

    public String toStirng() {
        return "Incorrect Login";
    }

}
