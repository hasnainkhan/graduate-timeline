package login;

import constants.Majors;
import constants.Quarter;

public abstract class LoginUser {

    public enum userType {

        student, advisor, administrator
    }

   final String resourcesFolder= this.getClass().getClassLoader().getResource("resources/").getPath();
   
     public final boolean vaidateLogin() {
       return  doesUserExist();
     }
    
     
    abstract String getIam();
    
    abstract void setIam(String iAm);
 
    public abstract String getFirstName();
    
    public abstract String getLastName();
    
      public abstract String getMajor();
    
    
    public abstract String getUserName(); 
    
    abstract void setUserName(String userName);
    
    abstract void setPassword(String password);
    
    abstract String getPassword();
    
     abstract boolean doesUserExist();
     
     abstract String getRecordsName();
     
     
    

}
