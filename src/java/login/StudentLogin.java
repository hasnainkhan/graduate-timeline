package login;

import registration.WebUser;
import com.google.gson.annotations.Expose;
import com.google.gson.stream.JsonWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.Writer;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class StudentLogin extends LoginUser {

    final private String recordsFIle;
    @Expose
    private String iAm;
    @Expose
    private String userName;
    @Expose
    private String password;
    private String firstName;
    private String lastName;
    private String major;

    public StudentLogin(String iAm, String userName, String password) {
        setIam(iAm);
        setUserName(userName);
        setPassword(password);

        this.recordsFIle = this.resourcesFolder + "records" + File.separator + this.getIam() + "-records";

        checkRecords();
    }

    public void checkRecords() {
        try {

            File file = new File(this.recordsFIle);

            if (file.createNewFile()) { // no file so create new one
                try (Writer writer = new FileWriter(file, true)) {
                    OutputStream out = new FileOutputStream(file, true);;
                    JsonWriter jwriter = new JsonWriter(writer);
                    jwriter.beginArray();

                    jwriter.endArray();
                    writer.flush();
                }
            } else {
                BufferedReader br = new BufferedReader(new FileReader(file));
                if (br.readLine() == null) {
                    try (Writer writer = new FileWriter(file, true)) {
                        OutputStream out = new FileOutputStream(file, true);;
                        JsonWriter jwriter = new JsonWriter(writer);
                        jwriter.beginArray();

                        jwriter.endArray();
                        writer.flush();
                    }

                }
                //"File already exists.
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * @return the iAm
     */
    @Override
    public String getIam() {
        return this.iAm;
    }

    /**
     * @param iAm the iAm to set
     */
    public void setIam(String iAm) {
        iAm = iAm.toLowerCase();
        for (userType in : userType.values()) {
            if (in.name().equals(iAm)) {
                this.iAm = iAm;
            }

        }

    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRecordsName() {

        return this.recordsFIle;

    }

    /**
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean doesUserExist() {

        File f = new File(this.getRecordsName());
        try {
            if (!f.exists()) {
            }

            FileInputStream is = new FileInputStream(f);
            String jsonTxt = IOUtils.toString(is);
            JSONArray jsonArray = new JSONArray(jsonTxt);
            System.out.println(jsonTxt);

            for (Object inArray : jsonArray) {
                if (inArray instanceof JSONObject) {
                    boolean usertest = false;
                    boolean passtest = false;
                    if (((JSONObject) inArray).getString("userName").equals(this.getUserName())) {
                        usertest = true;
                    }

                    if (((JSONObject) inArray).getString("password").equals(this.getPassword())) {
                        passtest = true;
                    }

                    if (usertest && passtest) {
                        String firstName = ((JSONObject) inArray).getString("firstName");
                        String lastName = ((JSONObject) inArray).getString("lastName");
                        String major = ((JSONObject) inArray).getString("major");
                        firstName= firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
                          lastName= lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
                           major = major.substring(0, 1).toUpperCase() +major.substring(1);
                        this.setFirstName(firstName);
                        this.setLastName(lastName);
                        this.setMajor(major);
                        return true;

                    }

                }

            }

            return false;
        } catch (Exception e) {

            return false;

        }

    }

    public String getFirstName() {

        return this.firstName;

    }

    public String getLastName() {

        return this.lastName;

    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the major
     */
    public String getMajor() {
        return major;
    }

    /**
     * @param major the major to set
     */
    public void setMajor(String major) {
        this.major = major;
    }

}
