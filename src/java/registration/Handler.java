   package registration;

import constants.Majors;
import constants.Quarter;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import registration.WebUser.userType;

public class Handler {

    private String type;
    private String iAm;
    private Majors major;
    private int coursePerQuarter;
    private String concentration;
    private Quarter startQuarter;
    private int startYear;
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private String birthDay;
    private String birthMonth;
    private String birthYear;
    private WebUser user1;

    public Handler() {
           this.iAm = null;
   this.major = null;
     this.coursePerQuarter = 0;
    this.concentration= null;
    this.startQuarter= null;
   this.startYear= 0;
    this.firstName= null;
    this.lastName= null;
    this.userName= null;
    this.password= null;
    this.birthDay= null;
    this.birthMonth= null;
    this.birthYear= null;

    }

    /**
     * @return the iAm
     */
    public String getiAm() {
        if (this.iAm == null)
            return "";
        return iAm;
    }

    /**
     * @param iAm the iAm to set
     */
    public void setiAm(String iAm) {
        
        for (userType in : userType.values()) {
            if (in.name().equals(iAm))
                this.iAm = iAm;
            
        }
        
      
        
       
    }

    /**
     * @return the major
     */
    public Majors getMajor() {
        if(this.major == null)
            return Majors.BIT;
        
        return major;
    }

    /**
     * @param major the major to set
     */
    public void setMajor(Majors major) {
        this.major = major;
    }

    /**
     * @return the coursePerQuarter
     */
    public int getCoursePerQuarter() {
        if (this.coursePerQuarter ==0)
            return 0;
        return coursePerQuarter;
    }

    /**
     * @param coursePerQuarter the coursePerQuarter to set
     */
    public void setCoursePerQuarter(int coursePerQuarter) {
        this.coursePerQuarter = coursePerQuarter;
    }

    /**
     * @return the concentration
     */
    public String getConcentration() {
        if (this.concentration == null)
            return "null-CON";
        return concentration;
    }

    /**
     * @param concentration the concentration to set
     */
    public void setConcentration(String concentration) {
        this.concentration = concentration;
    }

    /**
     * @return the startQuarter
     */
    public Quarter getStartQuarter() {
        if (this.startQuarter == null)
            return Quarter.Fall;
        
        return startQuarter;
    }

    /**
     * @param startQuarter the startQuarter to set
     */
    public void setStartQuarter(Quarter startQuarter) {
        this.startQuarter = startQuarter;
    }

    /**
     * @return the startYear
     */
    public int getStartYear() {
        if (this.startQuarter== null)
            return 0;
        return startYear;
    }

    /**
     * @param startYear the startYear to set
     */
    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the birthDay
     */
    public String getBirthDay() {
        return birthDay;
    }

    /**
     * @param birthDay the birthDay to set
     */
    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    /**
     * @return the birthMonth
     */
    public String getBirthMonth() {
        return birthMonth;
    }

    /**
     * @param birthMonth the birthMonth to set
     */
    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    /**
     * @return the birthYear
     */
    public String getBirthYear() {
        return birthYear;
    }

    /**
     * @param birthYear the birthYear to set
     */
    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }
    
    public void checkUser() {
        
//        if (userexists)
//           return string;
//        else
//            context();
    }
    
    
    public void context() {
        
        if (this.getType().equals("login")){
            System.out.print("JUST LOGIN");
            
        }
        
          if (this.getType().equals("registration")){
            register();
        }
       
        
    }
    
    public void register(){
         if (this.iAm.equals(userType.student.name()))
         user1 =new Student(this.getUserName(), this.getPassword(), this.getFirstName(),
            this.getLastName(), this.getMajor(), this.getConcentration(), this.getStartYear(),
           this.getStartQuarter(), this.getCoursePerQuarter(), this.getiAm(), this.getBirthDay(), this.getBirthMonth(), this.getBirthYear());
        try {
            user1.registerUser();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

}
