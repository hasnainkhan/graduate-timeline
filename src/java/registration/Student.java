package registration;

import com.google.gson.*;
import com.google.gson.annotations.Expose;
import com.google.gson.stream.JsonWriter;

import constants.Majors;
import constants.Quarter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Student extends WebUser {

    final private String recordsFIle;
    @Expose
    private userType iAm;
    @Expose
    private Majors major;

    private int coursePerQuarter;
    @Expose
    private String concentration;
    @Expose
    private Quarter startQuarter;
    @Expose
    private int startYear;
    @Expose
    private String firstName;
    @Expose
    private String lastName;
    @Expose
    private String userName;
    @Expose
    private String password;
    @Expose
    private String birthDay;
    @Expose
    private String birthMonth;
    @Expose
    private String birthYear;

    public Student(String userName, String password, String firstName,
            String lastName, Majors major, String conIn, int startYear,
            Quarter startQuarter, int coursePerQIn, String iAm, String birthDay, String birthMonth, String birthYear) {

        setUserName(userName);
        setPassword(password);
        setFirstName(firstName);
        setLastName(lastName);
        setMajor(major);
        setConcentration(conIn);
        setCoursePerQuarter(coursePerQIn);
        setStartYear(startYear);
        setStartQuarter(startQuarter);
        setCoursePerQuarter(coursePerQIn);
        setIam(iAm);
        setBirthDay(birthDay);
        setBirthMonth(birthMonth);
        setBirthYear(birthYear);

        this.recordsFIle = this.resourcesFolder +"records" + File.separator + this.iAm.name() + "-records";

    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setIam(String iAm) {
        this.iAm = this.iAm.valueOf(iAm);
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    @Override
    public void setFirstName(String firstName) {
        firstName = firstName.toLowerCase();
       firstName= firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
        this.firstName = firstName;
    }

    @Override
    public void setLastName(String lastName) {
        lastName= lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
        this.lastName = lastName;
    }

    @Override
    public void setStartQuarter(Quarter startQuarter) {
        this.startQuarter = startQuarter;
    }

    public void setCourse(Majors majorIn) {
        this.major = majorIn;
    }

    @Override
    public void setConcentration(String conIn) {

        this.concentration = this.major.getConcentration(conIn);

    }

    @Override
    public void setMajor(Majors majorIn) {
        this.major = majorIn;
    }

    @Override
    public void setCoursePerQuarter(int coursePerQIn) {
        this.coursePerQuarter = coursePerQIn;
    }

    @Override
    public String getUserName() {
        return this.userName;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public Majors getMajor() {
        return this.major;
    }

    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public String getConcentration() {
        return this.concentration;
    }

    @Override
    public int getCoursePerQuarter() {
        return this.coursePerQuarter;
    }

    @Override
    public String getMajorId() {
        return this.getMajor().name() + this.getConcentration();
    }

    @Override
    public Quarter getStartQuarter() {
        return this.startQuarter;
    }

    @Override
    public int getStartYear() {
        return startYear;

    }

    @Override
    public String getIam() {
        return this.iAm.name();
    }

    public String getRecordsName() {

        return this.recordsFIle;

    }

    /**
     * @return the birthDay
     */
    public String getBirthDay() {
        return birthDay;
    }

    /**
     * @param birthDay the birthDay to set
     */
    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    /**
     * @return the birthMonth
     */
    public String getBirthMonth() {
        return birthMonth;
    }

    /**
     * @param birthMonth the birthMonth to set
     */
    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    /**
     * @return the birthYear
     */
    public String getBirthYear() {
        return birthYear;
    }

    /**
     * @param birthYear the birthYear to set
     */
    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    @Override
    public void createUser() throws UserAlreadyExistsException {
        try {

        } catch (Exception e) {
            throw new UserAlreadyExistsException(this.userName);
        }

    }

    @Override
    public void saveToJson() throws UserAlreadyExistsException {

                Gson gson = null;
            //System.out.println(jsonTxt);

            gson = new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
                    .create();
           

            String json = gson.toJson(this);
            
        
      
     try (Writer writer = new FileWriter(this.getRecordsName(),true)) {
          OutputStream out = new FileOutputStream(this.getRecordsName(),true);;
            JsonWriter jwriter = new JsonWriter(writer);
              jwriter.beginArray();
           gson.toJson(this, writer);
         
           
     jwriter.endArray();
     writer.flush();
    String wholeFile = readFile(this.getRecordsName(), StandardCharsets.UTF_8);
    wholeFile = wholeFile.replace("][", ",");
    wholeFile = wholeFile.replace("]\n[", ",");
     Writer writer2 = new FileWriter(this.getRecordsName(),false);
    
    writer2.write(wholeFile);
         writer2.flush();


}
         catch  (Exception ex) {
           System.out.println(ex.getMessage());
        }
          
    
    
}
    
  static String readFile(String path, Charset encoding) 
  throws IOException 
{
  byte[] encoded = Files.readAllBytes(Paths.get(path));
  
  return new String(encoded, encoding);
}

}