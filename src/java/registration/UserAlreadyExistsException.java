/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registration;

/**
 *
 * @author hasnain
 */
public class UserAlreadyExistsException extends Exception {

    private String userName;

    public UserAlreadyExistsException(String userName) {
        this.userName = userName;
    }

    public String toStirng() {
        return "User name: " + this.userName + " already exists";
    }

}
