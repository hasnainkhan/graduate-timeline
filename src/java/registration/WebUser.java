package registration;

import constants.Majors;
import constants.Quarter;

public abstract class WebUser {

    public enum userType {
        
      student, advisor, administrator
    }

   final String resourcesFolder= this.getClass().getClassLoader().getResource("resources/").getPath();
   
    final public void registerUser() throws UserAlreadyExistsException {
        
        createUser();
        saveToJson();
         
       

    }
     
    
    abstract String getIam();
    
    abstract void setIam(String iAm);
 
    abstract void setStartQuarter(Quarter startQuarter);

    abstract void setMajor(Majors majorIn);

    abstract void setFirstName(String nameIn);

    abstract void setLastName(String nameIn);

    abstract void setConcentration(String conIn);

    abstract void setCoursePerQuarter(int coursePerQIn);
    
      abstract void setBirthDay(String birthDay);
      
        abstract void setBirthMonth(String birthMonth);
        
          abstract void setBirthYear(String birthYear);

    abstract Majors getMajor();

    abstract String getLastName();

   

    abstract String getConcentration();

    abstract int getCoursePerQuarter();

    abstract String getMajorId();

    abstract Quarter getStartQuarter();

    abstract int getStartYear();

    abstract void setStartYear(int startYear);
    
    abstract void createUser() throws UserAlreadyExistsException;
    
    abstract void saveToJson() throws UserAlreadyExistsException;
    
    abstract String getRecordsName();
    
    abstract String getUserName(); 
    
    abstract void setUserName(String userName);
    
    abstract void setPassword(String password);
    
    abstract String getPassword();
    
    abstract String getBirthDay();
      
        abstract String getBirthMonth();
        
          abstract String getBirthYear();
    

}
