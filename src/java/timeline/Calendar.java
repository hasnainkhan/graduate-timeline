/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timeline;

import java.util.ArrayList;

/**
 *
 * @author hasnain
 */
public class Calendar {
    
    private int coursePerQuarter;
	 
private String startQuarter;
private Quarter start;
private ArrayList<Quarter> path;
private ArrayList<Course> requirements;
private final int year = 2015;

public Calendar(int coursePerQuarter, String startQuarter, int startYear, ArrayList<Course> requirements) {
    this.start = QuarterFactory.getInstance().makeQuarter(startQuarter, coursePerQuarter, year);
    this.requirements = requirements;
    path = new ArrayList<>();
    path.add(start);
    
}

public int getNumberCoursesTaken(){
		int total = 0;
		for(Quarter x: path){
			total += x.getNumberCoursesTaken();
		}
		return total;
	}
	
	public void makePath(ArrayList<Course> requirments){
		int goal = requirments.size();
		int i = 0;
		Quarter current = start;
		while(goal != getNumberCoursesTaken()){
			Course delete;
			for(Course b1: requirments ){
				if(b1.CheckPreq(path) && (!current.hasCourseBeenTaken(b1.getId())) || b1.getElective() && b1.CheckSeason(current) && i < coursePerQuarter){
					delete = b1;
					try {
						current.addCourse(b1);
					} catch (Exception e) {
						e.printStackTrace();
					}
					i++;
					requirments.remove(delete);
					break;
				}
					if(i >= coursePerQuarter || requirments.indexOf(b1) == requirments.size()-1){ 
						i = 0;
						path.add(current);
						current = current.getNext();
						}
			}	
		}

	}
	
	public ArrayList<Quarter> getPath(){
		return path;
	}
    
        
        public String toString(){
            String booboo = "";
            for(Quarter x: path){
                booboo += x.information() + "\n";
            }
            return booboo;
        }
        
}
