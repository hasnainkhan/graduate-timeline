package timeline;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import org.json.JSONArray;

//import quarter.Quarter;
//import constants.Majors.CDM;

public class Course {
	
	private String subject;
	private int catalogNbr;
	private String id;
	private String majorName;
	private String concentration;
	private boolean offeredFall;
	private boolean offeredWinter; 
	private boolean offeredSpring;
	private boolean safe_fall_starter;
	private boolean safe_winter_starter;
	private boolean safe_spring_starter;
                   private ArrayList<String> course_preq;
	private JSONArray preq;
	private boolean elective;
	private boolean capstone;
	private String comments;
	private String url;
                  private String newpreq;
        
       
        
        
	
	public Course(String subject, int catalogNbr, String majorName,/*String concentration,*/
	  boolean offeredFall, boolean offeredWinter, boolean offeredSpring, 
	  boolean safe_fall_starter,  
	  boolean safe_winter_starter, boolean safe_spring_starter,  
	  String newpreq,  boolean elective,  boolean capstone, String comments, String url){
		
		setSubject(subject);
		setCatalogNbr(catalogNbr);
		id = this.getId(this.subject, this.catalogNbr);
		setMajorName(majorName);
		setConcentration(concentration);
		setOfferedFall(offeredFall);
		setOfferedWinter(offeredWinter);
		setOfferedSpring(offeredSpring);
		setSafeFallStarter(safe_fall_starter);
		setSafeWinterStarter(safe_winter_starter);
		setSafeSpringStarter(safe_spring_starter);
              //   setCourse_preq(preq);
                            setNewpreq(newpreq);
		setElective(elective);
		setCapstone(capstone);
		setComments(comments);
		setUrl(url);
	}
	

	
	public void setSubject(String subject) {
		this.subject = subject;
		
	}
	
	public String getId(String subject, int catalogNbr){
		return subject+catalogNbr;
	}
	
	public String getId(){
		return id;
	}
	
	public void setCatalogNbr(int catalogNbr) {
		this.catalogNbr = catalogNbr;
		
	}
	
   
   public void setMajorName(String majorName){
	   this.majorName=majorName;
   }
   
   public void setConcentration(String conIn) {
		this.concentration = conIn;
		
	}
   
   public void setOfferedFall(boolean offeredFall){
	   this.offeredFall = offeredFall;
	   
   }
   public void setOfferedWinter(boolean offeredWinter){
	   this.offeredWinter = offeredWinter;
   }
   public void setOfferedSpring(boolean offeredSpring){
	   this.offeredSpring = offeredSpring;
	   
   }
   public void setSafeFallStarter(boolean safe_fall_starter){
	   this.safe_fall_starter = safe_fall_starter;
	   
   }
   public void setSafeWinterStarter(boolean safe_winter_starter){
	   this.safe_winter_starter = safe_winter_starter;
	   
   }
   public void setSafeSpringStarter(boolean safe_spring_starter){
	   this.safe_spring_starter = safe_spring_starter;
	   
   }
   public void setPreq(JSONArray preq) {
	   this.preq = preq;
           
           ArrayList<String> list = new ArrayList<String>();     
JSONArray jsonArray = (JSONArray)preq; 
if (preq != null) { 
   int len = preq.length();
   for (int i=0;i<len;i++){ 
    list.add(preq.get(i).toString());
   } 
   this.setCourse_preq(list);
} 
	   
   }
  
   public boolean HasPrq(){
		if(course_preq.isEmpty()) return false;
		return true;
	}
/*
public void addPreq(String p)
	{
	String temp = p.substring(1, p.length()-1);
                    if(temp.equals(null)){}
                    else if(temp.indexOf(",") == -1){
			
		}
		else{
			while(temp.indexOf(",") != -1 ){
				String inter = temp.substring(0, p.indexOf(","));
//				if(inter.indexOf(" ") == 0 ){
//					inter = p.substring(1);
//				}
				if(m.hasCourse(inter)){
					preq.add(m.getCourse(inter));
				}
				else{
//					Course course = new Course(inter);
//					c.put(inter, course);
//					pre.add(course);
					//debug line
					System.out.println("System could not find " + temp);
				}
				temp = temp.substring(temp.indexOf(",")+1);
				if(temp.charAt(0) ==  ' ') temp = temp.substring(1);
			}
		}
	}
   
   */
                //checks that at least 1 of the course_prequists has been taken.
	public boolean CheckPreq(ArrayList<Quarter> path){
		if(course_preq.isEmpty()) return true;
		for(Quarter qr: path){
			for( String pr: course_preq){
				if(qr.hasCourseBeenTaken(pr)) return true;
		}		
		}
		return false;
	}
	
	public boolean CheckSeason(Quarter m){
		String now = m.getQuarter();
		if(now.equals("Fall") && this.getOfferedFall()){
			return true;
		}
		if(now.equals("Winter") && this.getOfferedWinter()){
			return true;
		}
		if(now.equals("Spring") && this.getOfferedSpring()){
			return true;
		}
		return false;
	}
	
 
	
   public void setElective(boolean elective) {
	   this.elective = elective;
	   
   }
   public void setCapstone(boolean capstone) {
	   this.capstone = capstone;
	   
   }
   public void setComments(String comments) {
	   this.comments = comments;
	   
   }
   public void setUrl(String url){
	   this.url = url;
	   
   }
   
   public String getSubject() {
		return this.subject ;
		
	}
	
	public int getCatalogNbr( ) {
		 return  this.catalogNbr  ;
		
	}
	
  
  public String getMajorName( ){
	   return this.majorName;
  }
  
  public String getConcentration(){
		return this.getConcentration();
	}
  
  public boolean getOfferedFall( ){
	  return  this.offeredFall  ;
	   
  }
  public boolean  getOfferedWinter(){
	  return  this.offeredWinter;
  }
  public boolean getOfferedSpring(){
	  return  this.offeredSpring  ;
	   
  }
  public boolean getSafeFallStarter( ){
	  return  this.safe_fall_starter ;
	   
  }
  public boolean  getSafeWinterStarter( ){
	   return this.safe_winter_starter ;
	   
  }
  public boolean getSafeSpringStarter( ){
	   return this.safe_spring_starter  ;
	   
  }
  public JSONArray getPreq( ) {
	  return  this.preq  ;
	   
  }
  public boolean getElective( ) {
	   return this.elective;
	   
  }
  public boolean getCapstone( ) {
	   return this.capstone ;
	   
  }
  public String getComments( ) {
	   return this.comments ;
	   
  }
  public String getUrl(   ){
	   return this.url ;
	   
  }

    /**
     * @return the course_preq
     */
    public ArrayList<String> getCourse_preq() {
        return course_preq;
    }

    /**
     * @param course_preq the course_preq to set
     */
    public void setCourse_preq(ArrayList<String> course_preq) {
        this.course_preq = course_preq;
    }

    /**
     * @return the newpreq
     */
    public String getNewpreq() {
        return newpreq;
    }

    /**
     * @param newpreq the newpreq to set
     */
    public void setNewpreq(String newpreq) {
       newpreq = newpreq.replace("[", "");
         newpreq = newpreq.replace("]", "");
         String[] split = newpreq.split("");
     this.course_preq =    new ArrayList( Arrays.asList( split ) );
        this.newpreq = newpreq;
    }
	 
}
