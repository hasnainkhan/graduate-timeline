/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timeline;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author hasnain
 */
public class CourseFactory {

   private ArrayList<Course> major = new ArrayList<>();

    public CourseFactory() {

    }

    public void json(File in) throws Exception {

        File f = new File(in.getPath());

        if (f.exists()) {
            FileInputStream is = new FileInputStream(f);
            String jsonTxt = IOUtils.toString(is);
            JSONArray jsonArray = new JSONArray(jsonTxt);
            //System.out.println(jsonTxt);

            for (Object inArray : jsonArray) {

                if (inArray instanceof JSONObject) {

                    getMajor().add(new Course(
                            ((JSONObject) inArray).getString("Subject"),
                            ((JSONObject) inArray).getInt("CatalogNbr"),
                            ((JSONObject) inArray).getString("Major"),
                           // ((JSONObject) inArray).getString("concentration"),
                            ((JSONObject) inArray).getBoolean("Fall"),
                            ((JSONObject) inArray).getBoolean("Winter"),
                            ((JSONObject) inArray).getBoolean("Spring"),
                            ((JSONObject) inArray).getBoolean("safe_fall_starter"),
                            ((JSONObject) inArray).getBoolean("safe_winter_starter"),
                            ((JSONObject) inArray).getBoolean("safe_spring_starter"),
                            ((JSONObject) inArray).getString("preq"),
                            // ((((JSONObject) inArray).isNull("preq")) ? "null" :  ((JSONObject) inArray).getString("preq")),

                            ((JSONObject) inArray).getBoolean("elective"),
                            ((JSONObject) inArray).getBoolean("capstone"),
                         String.valueOf(((JSONObject) inArray).get("comments")),
                           
                             String.valueOf(((JSONObject) inArray).get("url"))   ));
                    

                }

            }

        }

    }
    
    
    
    /*
    public static void main(String[] args){
        
        try {
        CourseFactory cf = new CourseFactory();
        File f = new File("/home/hasnain/Documents/DePaul/Capstone/Demo/src/java/resources/Majors/Network Engineering and Security/Network Engineering and Security");
 
        
            cf.json(f);
            for (int i=0; i<cf.getMajor().size();i++){
              System.out.println(cf.getMajor().get(i).getId());
                
            }
            
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }*/

    /**
     * @return the major
     */
    public ArrayList<Course> getMajor() {
        return major;
    }

    /**
     * @param major the major to set
     */
    private void setMajor(ArrayList<Course> major) {
        this.major = major;
    }

}
