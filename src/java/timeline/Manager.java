/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timeline;

import java.io.File;
import java.util.ArrayList;
import whatif.Major;

/**
 *
 * @author hasnain
 */
import whatif.*;
import graduatetimeline.*;
import graduatetimeline.driver.TimelineMain;
import graduatetimeline.timeline.Timeline;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Manager {
    
 //   private Handler courseInfo   ;//= new Handler();
    private String major;
    private String con;
    private String startQuarter;
    private String coursesPerQuarter;
    final String resourcesFolder= this.getClass().getClassLoader().getResource("resources/").getPath();
    private String message;
    
    public Manager() {
        
       // this.setCourseInfo(new whatif.Handler() );
        
    }

    /**
     * @return the courseInfo
     */
    public  Handler getCourseInfo() {
        return null;//courseInfo;
    }

    /**
     * @param courseInfo the courseInfo to set
     */
   private void setCourseInfo( Handler courseInfo) {
        
       // this.courseInfo = courseInfo;
        
    }
   
   public void major() {
      for (File f : this.getCourseInfo().getListOfFiles()) {
          System.out.println(f.getName());
          
      }
       
       
   }
    
    public void con(String in) {
        
        for (Major m : this.getCourseInfo().getListOfMajors()) {
             if (m.getName().equals(in)) {
                 
                for (File f : m.getListOfFiles()) {
                       System.out.println(f.getName());
                }
             }
          
      }
       
       
   }
    

    /**
     * @return the major
     */
    public String getMajor() {
        return major;
    }

    /**
     * @param major the major to set
     */
    public void setMajor(String major) {
        this.major = major;
    }

    /**
     * @return the con
     */
    public String getCon() {
        return con;
    }

    /**
     * @param con the con to set
     */
    public void setCon(String con) {
        this.con = con;
    }

    /**
     * @return the startQuarter
     */
    public String getStartQuarter() {
        return startQuarter;
    }

    /**
     * @param startQuarter the startQuarter to set
     */
    public void setStartQuarter(String startQuarter) {
        this.startQuarter = startQuarter;
    }

    /**
     * @return the coursesPerQuarter
     */
    public String getCoursesPerQuarter() {
        return coursesPerQuarter;
    }

    /**
     * @param coursesPerQuarter the coursesPerQuarter to set
     */
    public void setCoursesPerQuarter(String coursesPerQuarter) {
        this.coursesPerQuarter = coursesPerQuarter;
    }
    
    public void createPath() {
        
        try {
            TimelineMain.createStudent(this.getMajor(), this.getCon(), this.getStartQuarter(), this.getCoursesPerQuarter());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         
         
        
        
        
    }
    
    public void context() {
        
        /*
        
        CourseFactory cf = new CourseFactory();
        
       setFilePath(cf);
        
       ArrayList<Course> toTake = cf.getMajor();
      
       Calendar c = new Calendar(Integer.parseInt(this.getCoursesPerQuarter()),this.getStartQuarter(), 2015 , toTake);
       c.makePath(toTake);
       this.setMessage(c.toString()); */
       this.createPath();
    }
    
    public void setFilePath(CourseFactory cf) {
        try{
         String json = "Majors" + File.separator+ this.major + File.separator;
        if(this.con.equals("Standard")) {//major folder and file name 0;
        json +="0";   
        }
        
        else{
            json += this.con;
            
        }
        cf.json(new File(resourcesFolder+json));
        
        }
        catch (Exception e){
            
        }
    }
  

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
}
