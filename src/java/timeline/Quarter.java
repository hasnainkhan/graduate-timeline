package timeline;
 
 

import java.util.ArrayList;

//mport constants.Majors.CDM;

public class Quarter {

	private String quarter;
	private String major;
	private int coursePerQuarter;
	private int year;
	private String concentration;
	private int numberCoursesTaken;
	//private Course[] courseCart = new Course [coursePerQuarter];
	private ArrayList<Course> courseCart = new ArrayList<>();
	
	public Quarter(String startQIn, String concentration, String majorIn, int coursePerQIn, int yearIn){
		
		 setStartQuarter(startQIn);
		 setMajor(majorIn);
		 setConcentration(concentration);
		 setCoursePerQuarter(coursePerQIn );
		 setYear(yearIn);
		 numberCoursesTaken = 0;
		 
		  
	}
	
	
        public Quarter(String startQIn, int coursePerQIn, int yearIn){
		
		 setStartQuarter(startQIn);
		  
		 
		 setCoursePerQuarter(coursePerQIn );
		 setYear(yearIn);
		 numberCoursesTaken = 0;
		 
		  
	}
	
        
	public void setYear(int year){
		this.year = year;
	}
	public void setStartQuarter(String startQIn) {
		if(startQIn.equals("Fall") ||startQIn.equals("Winter") || startQIn.equals("Spring") ){
		this.quarter = startQIn;
		}
		else
			throw new NullPointerException("Quarter Name Invalid");
	}
	
	public void setConcentration(String conIn) {
		this.concentration = conIn;
		
	}
	
	public void setMajor(String majorIn) {
		this.major = majorIn;
	}
	
	public void setCoursePerQuarter(int coursePerQIn) {
		this.coursePerQuarter = coursePerQIn;
	}
	
	public String  getMajor( ) {
		return this.major;
	}
	
	public String getQuarter() {
		return this.quarter;
	}
	
	public int getCoursePerQuarter( ) {
		return this.coursePerQuarter;
	}
	
	public String getConcentration(){
		return this.getConcentration();
	}
	
	public void addCourse(Course courseIn) throws Exception{
		if (this.getCourseCart().size()>= this.getCoursePerQuarter())
			throw new Exception();
		this.getCourseCart().add(courseIn);
		numberCoursesTaken++;
	}
	
	public int getNumberCoursesTaken(){
		return numberCoursesTaken;
	}
	
	public ArrayList<Course> getCourseCart( ) {
		
		return this.courseCart;
		
	}
	
	public boolean hasCourseBeenTaken(String id){
		for (Course d : this.getCourseCart()){
                                                if(d.getId().equals(id))
			return true;
		}
                
		return false;
	}
	
	public String information() {
		StringBuilder builder = new StringBuilder();


		builder.append("\n--------------------Top-Quarter-Info-----------------------");
		builder.append("\nMajor: "+this.getMajor());
		builder.append("\n"+String.valueOf(this.getYear()) + " " + this.getQuarter());
		builder.append("\nCourses per quarter: "+ this.getCoursePerQuarter());
		builder.append("\nCourse slots left: " + (this.getCoursePerQuarter() - this.getCourseCart().size()));
		
		builder.append("\nCourses in this Quarter");
		
		for (Course in: this.getCourseCart()){
			builder.append("\n"+in.getSubject()+" "+ in.getCatalogNbr());
		}
		builder.append("\n-------------------Bottom-Quarter-Info-----------------------");
		System.out.println(builder.toString());
	return builder.toString();
	}
	
	public String id() {
		
		return this.getMajor() + this.getQuarter() + this.getCoursePerQuarter();
	}
	
	public boolean isCourseCartFull( ) {
		
		if (this.getCourseCart().size()>=this.getCoursePerQuarter())
			return true;
		else
			return false;
		
	}
	
	public int getYear(){
		return this.year;
	}
	
	public Quarter getNext(){
		QuarterFactory next = QuarterFactory.getInstance();
	switch (this.getQuarter()) {
    case "Fall" :  return next.makeQuarter("Winter", this.coursePerQuarter, (this.year +1));
    case "Winter":  return next.makeQuarter("Spring",  this.coursePerQuarter, this.year);
    case "Spring": return next.makeQuarter("Fall", this.coursePerQuarter, this.year);
    default:
    	throw new NullPointerException("Cannot find next quarter");
	}
	}
}
