 
package whatif;

import com.google.gson.stream.JsonWriter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.StringWriter;
import java.net.URLDecoder;

/**
 *
 * @author hasnain
 */
public class Handler {
  
    private File[] listOfFiles;
    private Major[] listOfMajors;
     final String resourcesFolder= this.getClass().getClassLoader().getResource("resources/").getPath();
    
    public Handler() {
        
        File folder = new File(this.resourcesFolder+"Majors");
        this.listOfFiles = folder.listFiles();
        this.setListOfMajors(this.listOfFiles);
        
        
    }
    
    public String[] con(String majorIn) {
        String[]  toClient = null;
        for (Major m: this.getListOfMajors()){
            if (m.getName().equals(majorIn)){
                toClient = new String[m.getListOfConcentrations().length];
                int i = 0;
                for (Concentration c: m.getListOfConcentrations()) {
                    toClient[i] = c.getName();
                    i++;
                }
                
            }
            
        }
        return toClient;
        
    }
    
    
    public String JSONcon (String majorin){
        try{
         StringWriter sw = new StringWriter();
  BufferedWriter bw = new BufferedWriter(sw);       
  JsonWriter writer = new JsonWriter(bw);
  writer.beginObject();
  writer.name("concentrations");
  writer.beginArray();
  for (String s: con(majorin)){
      if (!s.equals("0") && !s.contains("."))
         writer.value(s);
      
       if (s.equals("0"))
           writer.value("Standard");
       
    //   writer.value(s);
      
  }
  writer.endArray();
  writer.endObject();
  writer.flush();
  writer.close();
 // System.out.println(sw.toString());
        return sw.toString();
        }
        catch (Exception e){
            System.out.println(e);
            return null;
        }
        
    }
 
    
    public String jsonArray(){
        try{
            String array= null;
             StringWriter sw = new StringWriter();
  BufferedWriter bw = new BufferedWriter(sw);       
  JsonWriter writer = new JsonWriter(bw);
  writer.beginArray();     
  for (Major m: this.getListOfMajors()){
      bw.write(m.getJson().toString());
      bw.write(",");
  }
   writer.endArray();
  
  bw.flush();
  writer.flush();
  sw.flush();

  array = sw.toString();
 array = array.replace(",]", "]");
  
 
      return array;
        }
        catch (Exception e){
            
            return null;
            
        }
        
        
        
    }
    /**
     * @return thelistOfMajorss
     */
    public File[] getListOfFiles() {
        return listOfFiles;
    }

    /**
     * @return the listOfMajors
     */
    public Major[] getListOfMajors() {
        return listOfMajors;
    }

    /**
     * @param listOfMajors the listOfMajors to set
     */
    private void setListOfMajors(File[] listOfMajors) {
        Major[] majorResource = new Major[this.getListOfFiles().length];
        int i =0;
        for (File f: this.getListOfFiles()){
            
               try {
        String fixPath = f.getPath();
         fixPath =URLDecoder.decode(fixPath, "UTF-8");
            majorResource[i] = new Major(fixPath);
            i++;
               }catch (Exception e){
                    System.out.println(e.getMessage());
               }
         
         
        }
        this.listOfMajors = majorResource;
    }
    
    
     
    
}
