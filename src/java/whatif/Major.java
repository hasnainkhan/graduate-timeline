/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whatif;

import com.google.gson.stream.JsonWriter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.StringWriter;
import java.net.URLDecoder;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author hasnain
 */
public class Major {
       private File[] listOfFiles;
    private Concentration[] listOfConcentrations;
     final String resourcesFolder= this.getClass().getClassLoader().getResource("resources/").getPath();
     private String name;
     private JSONObject json;
     private JSONArray jsonArray;
     
    public Major(String in) {
         try {
             File major = new File(in);
         
        this.setName(major.getName());
        File folder = new File(major.getPath());
        
        this.listOfFiles = folder.listFiles();
        this.setListOfConcentrations(this.listOfFiles);
       this.setJson(major);
                  
                       } catch (Exception ex) {
               System.out.println(ex.getMessage());
           }
    }

    /**
     * @return the listOfConcentrations
     */
    public Concentration[] getListOfConcentrations() {
        return listOfConcentrations;
    }

    /**
     * @param listOfConcentrations the listOfConcentrations to set
     */
    private void setListOfConcentrations(File[] listOfFiles) {
        try{
         Concentration[] concentrationResource = new Concentration[this.getListOfFiles().length];
      for (int i =0; i<this.getListOfFiles().length; i++){
 //       if (!this.getListOfFiles()[i].getName().equals("0")){
            String fixPath = this.getListOfFiles()[i].getPath();
         fixPath =URLDecoder.decode(fixPath, "UTF-8");
            concentrationResource[i] = new Concentration(fixPath);
         
      }

       this.listOfConcentrations = concentrationResource;
        }catch (Exception e){System.out.println(e.getMessage());}
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
   private void setName(String name) {
        this.name = name;
    }

    /**
     * @param listOfConcentrations the listOfConcentrations to set
     */
    

    /**
     * @return the listOfFiles
     */
    public File[] getListOfFiles() {
        return listOfFiles;
    }
    
    private void setJson(File in) {
       File f = new File(in.getPath());
        try {
            if (!f.exists()) {}
            
  StringWriter sw = new StringWriter();
  BufferedWriter bw = new BufferedWriter(sw);       
  JsonWriter writer = new JsonWriter(bw);
  writer.beginObject();
  writer.name("major").value(this.getName());
  writer.name("concentrations");
  
writer.beginArray();
for (Concentration value: this.getListOfConcentrations()){
    if (!value.getName().equals("0") && !value.getName().contains("."))
    writer.value(value.getName());
    
       if (value.getName().equals("0"))
           writer.value("Standard");
    
     
}
writer.endArray();
writer.endObject();
writer.flush();
writer.close();
 this.json = new JSONObject(sw.toString());
 System.out.println(this.json);
    }catch (Exception e){
            System.out.println(e);
        }
            }

    /**
     * @return the json
     */
    public JSONObject getJson() {
        return json;
    }

    

    
}

